Below is the default .stow_global_ignore that is always turned on by default.
If there is a ~/.stow-global-ignore file, it will be used instead of the
the default version that is seen in a section below.

All ignore instructions are one per line, formatted as perl regex expressions.
(I think this is the same as the formattion for .gitignore files)

Additionally, you can put a .stow-local-ignore file in the root directory of a
directory to provide custom ignores which will be applied only to that
directory.

### Defualt .stow-global-ignore
```
# Comments and blank lines are allowed.

RCS
.+,v

CVS
\.\#.+       # CVS conflict files / emacs lock files
\.cvsignore

\.svn
_darcs
\.hg

\.git
\.gitignore

.+~          # emacs backup files
\#.*\#       # emacs autosave files

^/README.*
^/LICENSE.*
^/COPYING
```
