#!/usr/bin/env bash

export BASHPROFILE=1

if [ -z ${UNIX_PROGRAM} ]; then
    if [[ $(uname -a) =~ "WSL2" ]]; then
        export UNIX_PROGRAM="WSL2"
    elif [[ $(uname -a) =~ "WSL" ]]; then
        export UNIX_PROGRAM="WSL"
    elif [[ $(uname -a) =~ "CYGWIN" ]]; then
        export UNIX_PROGRAM="CYGWIN"
    else
        echo 'ERROR(~/.bash_profile): Could not determine $UNIX_PROGRAM.'
    fi
fi

# by default, Cygwin's standard launcher sets this to "mintty"
if [ -z ${TERM_PROGRAM} ]; then
    if [ ! -z ${WT_SESSION} ]; then
        export TERM_PROGRAM="WinTerm"
    fi
fi

# User dependent .bash_profile file

# source the users bashrc if it exists
if [ -f "${HOME}/.bashrc" ] ; then
  source "${HOME}/.bashrc"
fi

if [ -f "${HOME}/.bash_paths" ] ; then
  source "${HOME}/.bash_paths"
fi

if [ -f "${HOME}/.bash_aliases" ] ; then
  source "${HOME}/.bash_aliases"
fi

if [ -f "${HOME}/.bash_colors" ] ; then
  source "${HOME}/.bash_colors"
fi

# Set PATH so it includes user's private bin if it exists
if [ -d "${HOME}/bin" ] ; then
  PATH="${HOME}/bin:${PATH}"
fi

# set PATH so it includes user's private bin if it exists
# if [ -d "$HOME/.local/bin" ] ; then
#     PATH="$HOME/.local/bin:$PATH"
# fi

# Set MANPATH so it includes users' private man if it exists
# if [ -d "${HOME}/man" ]; then
#   MANPATH="${HOME}/man:${MANPATH}"
# fi

# Set INFOPATH so it includes users' private info if it exists
# if [ -d "${HOME}/info" ]; then
#   INFOPATH="${HOME}/info:${INFOPATH}"
# fi

if [ -f "$HOME/.bash_local" ] ; then
    source "${HOME}/.bash_local"
fi

set show-all-if-ambiguous on
