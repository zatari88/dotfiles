#!/usr/bin/env bash

export EDITOR="vim"
export VISUAL="vim"

# Default to human readale figures
alias df='df -h'
alias du='du -h'

# Grep with color
alias grep='grep --color'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Directory listing shortcuts with color
alias ls='ls -hF --color=tty'
alias {ll,la}='ls -lah'

alias dir='ls --color=auto --format=vertical'
alias vdir='ls --color=auto --format=long'

# alias v and vi to vim
alias {v,vi,im}='vim'
alias {vr,vir,vimr}='vim -R' # -R = readonly

# Start Windows command prompt from cygwin
alias cmd='cmd /c start cmd'

# Delete all swap file in the current directory
alias rmswp='rm ./.*.swp'

# Change directory and list the files in it
cdl  () { cd $1; ls; }
cdla () { cd $1; la; }

# Make backing up directory tree easier
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias .......="cd ../../../../../.."
alias ........="cd ../../../../../../.."
alias .........="cd ../../../../../../../.."
alias ..........="cd ../../../../../../../../.."
alias ...........="cd ../../../../../../../../../.."
alias ............="cd ../../../../../../../../../../.."
alias .............="cd ../../../../../../../../../../../.."

# Stow aliases
alias unstow='stow --delete'
alias restow='stow --restow' #unstow followed by stow. Used to prune old symlinks

# When starting tmux in cygwin running in Windows Terminal, use this alias:
if [[ $UNIX_PROGRAM == 'CYGWIN' ]]; then
    if [[ $TERM_PROGRAM == 'WinTerm' ]]; then
        alias tmux='script -c tmux /dev/null'
    fi
fi

findFile() {
    if [[ $1 == '' ]] || [[ $2 == '' ]]; then
        echo 'Useage: findFile <path> <iname>'
    else
        # 2>&1 pushes stderr through stdout then grep -v removes messages with
        # 'Permission denied' in the text
        find $1 -iname $2 2>&1 | grep -v "Permission denied"
    fi
}

findFileExact() {
    if [[ $1 == '' ]] || [[ $2 == '' ]]; then
        echo 'Useage: findFile <path> <name>'
    else
        find $1 -name $2 2>&1 | grep -v "Permission denied"
    fi
}

createSeq() {
    START=$1
    END=$2
    STEP=$3

    if [[ $START == '' ]]; then
        echo 'Useage: createSeqCmd <END>'
        echo 'Useage: createSeqCmd <START> <END(inclusive)>'
        echo 'Useage: createSeqCmd <START> <END(inclusive)> <STEP>'
        echo ''
        echo 'NOTE: createSeqCmd help is printed by multiple sequnce generation commands'
        echo 'NOTE: Default value of START=0'
        echo 'NOTE: Default value of STEP=1'
        return 1
    fi

    if [[ $END == '' ]]; then
        END=$START
        START=0
    fi
    if [[ $STEP == '' ]]; then
        STEP=1
    fi

    for ((i=$START; i<=$END; i+=$STEP)); do
        printf "$i "
    done
}

printSeqAsBytes() {
    local SEQ=$(createSeq)
    echo $SEQ
    for i in $SEQ; do
        printf "%02x" $i
    done
    echo '' #newline
}

fixWSL2ResolutionErrors() {
    printf "\n[network]\ngenerateResolvConf=false\n" | sudo tee -a /etc/wsl.conf
    printf "nameserver 8.8.8.8\n" | sudo tee -a /etc/resolv.conf
}
