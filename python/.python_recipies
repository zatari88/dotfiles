#!/usr/bin/env python3

###############################################################################
## Virtual Environments
###############################################################################

# Basically a container for running python scripts.

# NOTE: you never actually need to activate venvs to use them. If you run the
# python executable created during the creation of the venv, then that venv will
# will be used for the execution of that script.
# NOTE: This means that you can set the **Shebang** in the script to enforce
# the use of that venv during the execution of the script.

#------------------------------------------------------------------------------
# No activation use example using pre-made venv:
#------------------------------------------------------------------------------

#listing of script.py
```
#! venv/bin/python

print('This script is run from inside the venv due to its shebang')
```

python -m venv venv #create a new venv named venv
venv/bin/python -m pip install -r requirements.txt

#run the script using either
./script.py
venv/bin/python script.py

## NOTE: about shebang:
# The shebang in the listing only works if the pwd of the usre running the
# script is in the right location. There is an alternative form of the shebang
# which uses the env utility to resolve the location of the script and then
# run the script relative to that location, but support for this is not yet
# widespread (as of 2024, should work on recent Linux, Minux, and Cygwin).

#!/usr/bin/env -S /bin/sh -c '"$(dirname "$0")/venv/bin/python" "$0" "$@"'

#------------------------------------------------------------------------------
# Typical Use
#------------------------------------------------------------------------------

# create a venv
python -m venv exampleVenv

# activate a venv (make all the python commands use this venv)
(windows) exampleVenv\Scripts\activate
(Linux) source exampleVenv/bin/activate

# prompt will change to look like:
(exampleVenv) $

# with the venv active, just install stuff like normal with pip and run scripts
# like normal just calling python (which will use all the local stuff to this
# venv)

# create file which shows everything a new venv would need to install to
# replicate this venv
python -m pip freeze > requirements.txt

# replicate the venv described by the requirements.txt file
# (run from inside the active venv)
python -m pip install -r requirements.txt

# Show all packages installed (including those installed by default)
python -m pip list

#exit the venv by running the command
deactivate

###############################################################################
## use 'with' statements, but only sometimes
###############################################################################

### Easy way:

# This operates just the same as a standard with statement. When the code block
# is exited, the context is used to close the object. You can optionally
# provide a default value which doesn't have a close context by passing it into
# the nullcontext object.

from contextlib import nullcontext

f1Name = $(sometimes None, sometimes a real path}
f2Name = $(sometimes None, sometimes a real path}

with open(f1Name, 'r') if f1Name is not None else nullcontext() as f1:
    with open(f2Name, 'r') if f2Name is not None else nullcontext(altVal) as f2:
        #use f1 and f2 as needed....


### Harder but more flexible way:

# This operates by creating a stack object which holds all the context objects.
# When the stack object goes out of scope, it pops each of the contexts off in
# order and calls their close. This makes the above possible but also makes
# patterns where the above couldn't work possible as well.

from contextlib import ExitStack

f1Name = $(sometimes None, sometimes a real path}
f2Name = $(sometimes None, sometimes a real path}

with ExitStack as estack:
    f1 = None
    if f1Name is not None:
        f1 = estack.enter_context(open(f1Name, 'r')
    f2 = altVal
    if f2Name is not None:
        f2 = estack.enter_context(open(f2Name, 'r')

    #use f1 and f2 as needed...
