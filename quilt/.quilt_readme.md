Quilt
---
For more info: <https://wiki.debian.org/UsingQuilt>

This is used in reference to quit, which manages patches applied to the upstream
source code for a specific version of a debian package (or maybe just patches
generally?).

The files that quilt typically relies on in a debian package situation are
located in [debian-pkg]/debian/patches. In this folder, there are a number of
quilt formatted patches (using the standard diff format). There is additionally
a file named 'series' which contains the names of each of the patches, on per
line, in an ordered list of when each patch is applied.

`quilt series` will print the series of patches which are known by quilt
`quilt applied` will print the stack of patches which have already been applied
`quilt unapplied` will print the stack of patches not yet been applied
`quilt next` will print the patch which will be applied by the next push
`quilt push` will apply the next patch
`quilt pop` will remove the most recently applied patch
`quilt push -a` will push all unapplied patches in the series
`quilt pop -a` will pop all applied patches

Modifying a patch
---
Quilt only ever modifies whichever patch was __last__ applied. That means if you
want to modify a patch, then you just push every patch upto and including it,
then edit the files which should be further modified by this patch.
- `quilt push ...`
- ...
- `quilt edit file-which-should-be-further-changed-by-this-patch.ext`
- `quilt refresh myPatch.ext`
- `quilt pop -a`

Creating a new patch
---
`quilt new name-of-my-patch.ext` (typically .patch .diff or no ext.) will create a new empty patch and will push it.
`quilt edit file-to-be-modified-by-this-patch.ext` (will prep this file for changes and open it up for editing in your default editor)
    `quilt add file-to-be-modified.ext` will prep this file for changes, but will not open up the editor. If you don't use the `quilt edit` command, you must use this command prior to changing the file so that quilt can create a copy so it will know what changed.
`quilt diff` to review the current changes for this patch
`quilt refresh` will write out the current changes for this patch
(optional) `quilt header --dep3 -e` edit the header in $EDITOR (also include optional dep3 meta-info)
`quilt pop -a` return the source to the unpatched state to complete editing patches.
