vim9script

if exists("g:loaded_disallow_editing_folders")
	finish
endif

def DisallowEditingFolders()
	for f in argv()
		if isdirectory(f)
			echomsg "disallow_editing_folders.vim: Cowardly refusing to edit directory " . f
			quit
		endif
	endfor
enddef

g:loaded_disallow_editing_folders = 1
