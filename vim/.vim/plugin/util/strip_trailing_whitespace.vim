function StripTrailingWhitespace()
	if !&binary && &filetype != 'diff'
		normal mz
		normal Hmy
		%s/\s\+$//e
		normal 'yz<CR>
		normal `z
	endif
endfunction

"nnoremap <Plug>StripTrailingWhitespace :<C-U>call <SID>Strip()<CR>

"if !hasmapto('<Plug>StripTrailingWhitespace;')
	"nmap <unique> <Leader>uw <Plug>StripTrailingWhitespace;
"endif

"vim9script
"
"if exists("g:loaded_strip_trailing_whitespace")
"	finish
"endif
"
"
"def StripTrailingWhitespace()
"	if !&binary && &filetype != 'diff'
"		normal mz
"		normal Hmy
"		%s/\s\+$//e
"		normal 'yz<CR>
"		normal `z
"	endif
"enddef
"
"if !hasmapto('<Plug>StripTrailingWhitespace;')
"	map <unique> <Leader>uw <Plug>StripTrailingWhitespace;
"endif
"
"g:loaded_strip_trailing_whitespace = 1
